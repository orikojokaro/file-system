#include "myfs.h"
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <algorithm>
#define MAX_VAL_SIZE 10
const char *MyFs::MYFS_MAGIC = "MYFS";
void MyFs::printData()
{
	dataIter iter = data.begin();
	std::cout <<std::endl;
	for (; iter != data.end(); iter++)
	{
		std::cout << **iter << std::endl;
	}
}
//finds a / in or after the i th element, and returns a string from the i th element to the / excluding the /.
std::string findNextSlash(unsigned int i, const std::string* allData)
{
	unsigned int k = i;
	unsigned int size = allData->size();
	while (k < size  && k < i + MAX_ENTRY_SIZE)
	{
		char a = (*allData)[k];
		if ((*allData)[k] == '/')
		{
			return allData->substr(i, k-i);
		}
		k++;
	}
	throw(std::string("no // found, data might be currupt"));
	return std::string("");
}
//inserts a data obj into data.
void MyFs::insertIntoData(fileData * dataObj)
{
	bool wasInserted = false;
	if (data.size() == 0)
	{
		data.push_back(dataObj);
	}
	else
	{
		dataIter iter = data.begin();
		for (; iter != data.end(); iter++)
		{
			if ( *dataObj < **iter )
			{
				data.insert(iter, dataObj);
				wasInserted = true;
				break;
			}
		}
		if (! wasInserted)
		{
			data.push_back(dataObj);
		}
	}
}
void MyFs::createTable()
{
	std::string allData = useRead(blkdevsim->DEVICE_SIZE -TABLE_SIZE, TABLE_SIZE);
	fileData* dataObj = 0;
	for (unsigned int i = 0; i < TABLE_SIZE && allData[i] != '/'; )//each iteration is one entry.
	{
		dataObj = new fileData();
		std::string temp;
		std::string name = findNextSlash(i, &allData);
		i += name.size() + 1;//+1 to skip the slash.
		temp = findNextSlash(i , &allData);
		dataObj->setStart(atoi(temp.c_str()));
		i+= temp.size() + 1;
		temp = findNextSlash(i , &allData);
		dataObj->setSize(atoi(temp.c_str()));
		i+= temp.size() + 1;
		table.insert(std::pair<std::string, fileData*>(name, dataObj));
		insertIntoData(dataObj);
	}

}

MyFs::MyFs(BlockDeviceSimulator *blkdevsim_):blkdevsim(blkdevsim_) {
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);

	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION)) {
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
	else
	{
		createTable();
	}
}
MyFs::~MyFs()
{
	dataIter iter = data.begin();
	for (; iter != data.end(); iter++)
	{
		free(*iter);
	}
}
//complier is broken and doesn't include to_string in std.
std::string new_to_string(unsigned int value)
 {
   std::ostringstream os ;
   os << value ;
   return os.str() ;
 }
void MyFs::saveChanges()
{
	std::string allData = "";
	tableIter iter = table.begin();
	unsigned int startLoc = 0;
	for(;iter != table.end(); iter++)
	{
		allData += iter->first + "/";// using "/" because it is a name not allowed in linux and I don't need to implement folders,
		//so I can space the datas with it and thus achieve good memory efficiency.
		allData = allData + new_to_string(iter->second->getStart()) + "/";
		allData = allData + new_to_string(iter->second->getSize()) + "/";
	}
	allData = allData + "/";
	if (allData.size() > TABLE_SIZE)
	{
		throw(std::string("Not enough space."));
	}
	startLoc = blkdevsim->DEVICE_SIZE - TABLE_SIZE;
	blkdevsim->write(startLoc, allData.size(), allData.c_str());
}
bool isValidFileName(std::string name)
{
	return name.find('/') == std::string::npos;
}
void MyFs::format() {

	// put the header in place
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	blkdevsim->write(0, sizeof(header), (const char*)&header);

	// TODO: put your format code here
}
//Starts checking from currIter.
//returns an iterator after which there is space, and an integer describing how much space there is.
std::pair<dataIter,unsigned int> MyFs::findEmptyPart(dataIter currIter)
{
	int freeSize;
	dataIter nextIter = currIter;
	nextIter++;
	for (; currIter != data.end(); currIter++, nextIter++)
	{
		if (nextIter != data.end())
		{
			freeSize = (*nextIter)->getStart() - ((*currIter)->getStart() + (*currIter)->getSize());
			if (freeSize > 0)//if there is empty space between existing files
			{
				return std::pair<dataIter, unsigned int>(currIter, freeSize);
			}
		}
		else//if there wasn't space between files.
		{
			freeSize = blkdevsim->DEVICE_SIZE - sizeof(myfs_header) - (*currIter)->getStart() - (*currIter)->getSize() - TABLE_SIZE;
			if (freeSize > 0) //if there is space after the last file.
			{
				return std::pair<dataIter, unsigned int>(currIter, freeSize);
			}
			else//if there is no space after the last file.
			{
				throw(std::string("No space is empty"));
				return std::pair<dataIter, unsigned int>(currIter, 0);
			}
		}
	}
	//if it reached this part, it means currIter is the end.
	throw(std::string("Iter given is end()"));
	return std::pair<dataIter, unsigned int>(currIter, 0);
}
void MyFs::create_file(std::string path_str, bool directory) {
	struct myfs_header header;
		strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
		header.version = CURR_VERSION;
	if (directory)
	{
		throw (std::string("directory not implemented"));
	}
	else if (! isValidFileName(path_str))
	{
		throw(std::string("invalid file name - must not include /"));
	}
	dataIter currIter = data.begin();
	if (currIter == data.end())//if there are no files
	{
		fileData * newFileData = new fileData(sizeof(header) + 1, 0);
		data.insert(++currIter,newFileData);
		table.insert(std::pair<std::string, fileData*>(path_str,newFileData));
	}
	else
	{
		std::pair<dataIter, unsigned int>  emptyPart = findEmptyPart(currIter);
		if (emptyPart.second == 0)
		{
			throw(std::string("No space left"));
		}
		fileData * newFileData = new fileData((*emptyPart.first)->getStart() + (*emptyPart.first)->getSize(), 0);
		++(emptyPart.first);
		data.insert(emptyPart.first,newFileData);
		table.insert(std::pair<std::string, fileData*>(path_str,newFileData));
	}
}

std::string MyFs::useRead(unsigned int start, unsigned int size)
{
	if (size == 0)
	{
		return std::string("");
	}
	std::string result;
	result.resize(size, 0);
	blkdevsim->read(start,size, &result[0]);
	return result;
}

std::string MyFs::get_content(std::string path_str) {
	std::string buffer = "";
	std::pair<tableIter, tableIter> iters = table.equal_range(path_str);
	if (iters.first == iters.second)
	{
		throw (std::string("No file with the given name"));
	}

	if (iters.first == --iters.second)//if there is only one continous part in which the file was stored.
	{
		return useRead(iters.first->second->getStart(), iters.first->second->getSize());
	}
	else
	{
		iters.second++;
		for (tableIter iter = iters.first; iter != iters.second; iter++)//adding all segments.
		{
			buffer = buffer + useRead(iter->second->getStart(), iter->second->getSize());
		}
		return buffer;
	}
}

void MyFs::set_content(std::string path_str, std::string content) {
	int subUntil = 0;
	struct myfs_header header;
		strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
		header.version = CURR_VERSION;
	dataIter iter = data.begin(); //not std::find(data.begin(), data.end(), &(iters.second->second)); because we don't care if this is before or after the current segments.
	if (! isValidFileName(path_str))
	{
		throw(std::string("invalid file name - must not include /"));
	}
	//since files can't be deleted, only edited, we know the first file would start right after the header.
	content = content.substr(0, content.size() - 1);//removing the \n
	if (table.size() == 0)
	{

		blkdevsim->write(sizeof(header) +1, content.size(), content.c_str());
		fileData * dataObj = new fileData(sizeof(header) +1, content.size());
		table.insert(std::pair<std::string, fileData*> (path_str, dataObj));//inserting the new change in the table.
		data.push_back(dataObj);//inserting the new change into the data list.
		return;
	}
	while (content.size() > 0)
	{
		try//find empty part might throw if invalid parameters are passed or no memory is available
		{
			std::pair<dataIter, unsigned int> emptyPart = findEmptyPart(iter);//getting an empty part.
			iter = emptyPart.first;
			subUntil = content.size() > emptyPart.second ? emptyPart.second:content.size();//getting the length of chars that can be copied.
			fileData * dataObj = new fileData((*iter)->getStart() + (*iter)->getSize(), subUntil);
			blkdevsim->write((*iter)->getStart() + (*iter)->getSize(), subUntil, content.substr(0,subUntil).c_str());
			content = content.substr(subUntil, std::string::npos);//removing the copied part from the string.
			table.insert(std::pair<std::string, fileData*> (path_str, dataObj));//inserting the new change in the table.
			iter++;
			data.insert(iter, dataObj);//inserting the new change into the data list.
		}
		catch(std::string& error)
		{
			std::cout << error;
		}
	}
}

MyFs::dir_list MyFs::list_dir(std::string path_str) {
	using namespace std;
	dir_list ans;
	tableIter iter = table.begin();
	tableIter prevIter = table.begin();
	unsigned int totalSize = 0;
	if (path_str.length() != 1)//if there were no arguments, which is the only case supported the input would be "\"
	{
		throw std::string("not implemented");
	}
	else if(iter == table.end())
	{
		return ans;
	}
	iter++;
	for (;prevIter != table.end(); iter++,prevIter++ )
	{
		if (iter->first != prevIter->first)//if the file isn't segmented
		{
			dir_list_entry file = { prevIter->first, false, (int)prevIter->second->getSize()};
			ans.push_back(file);
		}
		else//if the file is segmented
		{
			while (prevIter->first == iter->first)
			{
				totalSize = prevIter->second->getSize();
				totalSize += iter->second->getSize();
				prevIter++;
				iter++;
			}
			dir_list_entry file = { prevIter->first, false, totalSize};
			ans.push_back(file);
		}
	}
	return ans;
}

std::ostream& operator<<(std::ostream& os,const  fileData& data){
std::cout << data._start << ", " << data._size;
return os;
 }
